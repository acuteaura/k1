.PHONY: default infra

default:

tf-init:
	terraform init

tf-apply: tf-init
	terraform apply

output.json:
	terraform output -json > output.json

apply: output.json
	kubeone apply -y -m kubeone.yaml -t output.json --force-upgrade

reset: output.json
	kubeone reset -m kubeone.yaml -t output.json
